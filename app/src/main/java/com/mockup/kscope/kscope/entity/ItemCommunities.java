package com.mockup.kscope.kscope.entity;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class ItemCommunities {

    private String name;
    private String logo;

    public ItemCommunities(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}

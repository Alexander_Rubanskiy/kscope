package com.mockup.kscope.kscope.fragments.signup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.SignupActivity;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import butterknife.OnClick;

/**
 * Created by MPODOLSKY on 12.06.2015.
 */
public class SignupLastFragment extends BaseFragment {

    public static Fragment newInstance() {
        SignupLastFragment fragment = new SignupLastFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((SignupActivity)getActivity()).setTitle(getString(R.string.signup_title_complete));
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_signup_complete;
    }

    @OnClick(R.id.save_and_close)
    public void saveAndClose(){
        ((SignupActivity)getActivity()).saveAndClose();
    }

    @OnClick(R.id.save_and_login)
    public void saveAndLogin(){
        ((SignupActivity)getActivity()).saveAndLogin();
    }
}

package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ListView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.MainActivity;
import com.mockup.kscope.kscope.adapters.MainMenuListAdapter;
import com.mockup.kscope.kscope.entity.ItemMainMenu;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentMainMenu extends BaseFragment {

    @InjectView(R.id.main_menu_list)
    ListView lvMainMenu;

    private String[] ArrayMainList = {"My profile", "Communities", "Calendar of events", "About us", "Testimonials"};

    private String title;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MainMenuListAdapter adapter = new MainMenuListAdapter(getActivity(), initList());
        adapter.setCallback(new MainMenuListAdapter.Callback() {
            @Override
            public void onMenuClick(int position) {
                addFragment(position);
            }
        });
        lvMainMenu.setAdapter(adapter);

    }

    private ArrayList<ItemMainMenu> initList() {
        ArrayList<ItemMainMenu> items = new ArrayList<>();
        for (int i = 0; i < ArrayMainList.length; i++) {
            ItemMainMenu itemMainMenu = new ItemMainMenu(ArrayMainList[i]);
            items.add(itemMainMenu);
        }
        return items;
    }

    private void addFragment(int position) {
        Fragment fragment = getFragment(position);
        ((MainActivity) getActivity()).addFragment(fragment, title);
    }

    private Fragment getFragment(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment =  new FragmentProfile();
                title = "My profile";
                break;
            case 1:
                fragment =  new FragmentCommunities();
                title = "Communities";
                break;
            case 2:
                fragment = new FragmentCalendar();
                title = "Calendar of events";
                break;
            case 3:
                fragment =  new FragmentAbout();
                title = "About us";
                break;
            case 4:
                fragment = new FragmentTestimonials();
                title = "Testimonials";
                break;
            default:
                break;
        }
        return fragment;
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_main;
    }

}
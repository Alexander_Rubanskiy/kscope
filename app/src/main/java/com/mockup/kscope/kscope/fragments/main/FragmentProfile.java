package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.fragments.BaseFragment;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentProfile extends BaseFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_profile;
    }

}
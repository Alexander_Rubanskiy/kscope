package com.mockup.kscope.kscope.fragments.main;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.adapters.TestimonialsAdapter;
import com.mockup.kscope.kscope.entity.ItemTestimonial;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;

import butterknife.InjectView;

/**
 * Created by Alexander Rubanskiy on 12.06.2015.
 */

public class FragmentTestimonials extends BaseFragment {

    @InjectView(R.id.list_testimonials)
    ExpandableListView lvTestimonials;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TestimonialsAdapter adapter = new TestimonialsAdapter(getActivity(), initDummyData());
        lvTestimonials.setAdapter(adapter);
        lvTestimonials.setGroupIndicator(null);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_testimonials;
    }

    private ArrayList<ItemTestimonial> initDummyData() {
        ArrayList<ItemTestimonial> about = new ArrayList<>();
        ItemTestimonial testimonial1 = new ItemTestimonial("Collete Philips","15:39 - 15/12/2014", "Very very long text in this field. Very very long text in this field. Very very long text in this field. Very very long text in this field.");
        ItemTestimonial testimonial2 = new ItemTestimonial("Boston Chamber of Commerce", "15:39 - 15/12/2014", "And in this field we have very long text. And in this field we have very long text. And in this field we have very long text. And in this field we have very long text.");
        ItemTestimonial testimonial3 = new ItemTestimonial("Mayor of Boston", "15:39 - 15/12/2014", "And in this field we have very long text. And in this field we have very long text. And in this field we have very long text. And in this field we have very long text.");
        about.add(testimonial1);
        about.add(testimonial2);
        about.add(testimonial3);
        return about;
    }

}
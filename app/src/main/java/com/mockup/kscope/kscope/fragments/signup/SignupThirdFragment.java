package com.mockup.kscope.kscope.fragments.signup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ListView;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.SignupActivity;
import com.mockup.kscope.kscope.adapters.SignupCommunitiesAdapter;
import com.mockup.kscope.kscope.entity.ItemCommunities;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import java.util.ArrayList;

import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by MPODOLSKY on 12.06.2015.
 */
public class SignupThirdFragment extends BaseFragment {

    @InjectView(R.id.list)
    ListView mList;

    private String[] ArrayCommunities = {"Community 1", "Community 2",
            "Community 3", "Community 4",
            "Community 5", "Community 6"};

    public static Fragment newInstance() {
        SignupThirdFragment fragment = new SignupThirdFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((SignupActivity)getActivity()).setTitle(String.format(getString(R.string.signup_title_n), SignupActivity.THIRD_FRAGMENT_ID));
        SignupCommunitiesAdapter adapter = new SignupCommunitiesAdapter(getActivity(), initList());
        mList.setAdapter(adapter);
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_signup_p3;
    }

    @OnClick(R.id.next)
    public void next(){
        ((SignupActivity)getActivity()).setFragment(SignupActivity.FOURTH_FRAGMENT_ID);
    }

    private ArrayList<ItemCommunities> initList() {
        ArrayList<ItemCommunities> items = new ArrayList<>();
        for (int i = 0; i < ArrayCommunities.length; i++) {
            ItemCommunities itemMainMenu = new ItemCommunities(ArrayCommunities[i]);
            items.add(itemMainMenu);
        }
        return items;
    }
}

package com.mockup.kscope.kscope.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.mockup.kscope.kscope.R;


/**
 * Created by MPODOLSKY on 12.06.2015.
 */

public class SplashActivity extends BaseActivity {

    public static final int DEFAULT_SPLASH_TIME = 2000;

    private Handler mHandler;
    private Runnable mStartActivityRunnable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mHandler = new Handler();
        mStartActivityRunnable = new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, SignupActivity.class));
                SplashActivity.this.finish();
            }
        };
        mHandler.postDelayed(mStartActivityRunnable, DEFAULT_SPLASH_TIME);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mHandler.removeCallbacks(mStartActivityRunnable);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }
}

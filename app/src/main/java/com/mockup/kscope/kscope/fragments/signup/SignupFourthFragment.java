package com.mockup.kscope.kscope.fragments.signup;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.mockup.kscope.kscope.R;
import com.mockup.kscope.kscope.activities.SignupActivity;
import com.mockup.kscope.kscope.fragments.BaseFragment;

import butterknife.OnClick;

/**
 * Created by MPODOLSKY on 12.06.2015.
 */
public class SignupFourthFragment extends BaseFragment {

    public static Fragment newInstance() {
        SignupFourthFragment fragment = new SignupFourthFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((SignupActivity)getActivity()).setTitle(String.format(getString(R.string.signup_title_n), SignupActivity.FOURTH_FRAGMENT_ID));
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_signup_p4;
    }

    @OnClick(R.id.next)
    public void next(){
        ((SignupActivity)getActivity()).setFragment(SignupActivity.LAST_FRAGMENT_ID);
    }
}
